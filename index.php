<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require __DIR__ . '/vendor/autoload.php';
require 'config/paths.php';
require 'config/const.php';
require 'config/database.php';
$app = new \Youtube\libs\Bootstrap();
