<?php

namespace Youtube\Helper;

class Helper
{
    public function url()
    {
        return sprintf(
                "%s://%s%s",
                isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
                $_SERVER['SERVER_NAME'],
                $_SERVER['REQUEST_URI']
        );
    }

    public function unsetCookie($cookie)
    {
        if (isset($_COOKIE[$cookie])) {
            unset($_COOKIE[$cookie]);
            setcookie($cookie, null, -1, '/');

            return true;
        }
        return false;
    }
}
