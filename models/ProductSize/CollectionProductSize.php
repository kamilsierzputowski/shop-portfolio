<?php

namespace Youtube\models\ProductSize;
use Youtube\libs\Database;
use Youtube\models\ProductSizeModel;

class CollectionProductSize
{
    protected const TABLE_NAME_PRODUCT_SIZE = 'product_size';
    protected $productSizeData = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getProductSizeData(): array
    {
        return $this->productSizeData;
    }

    public function setProductSizeData(array $productSizeData)
    {
        $this->productSizeData = $productSizeData;
    }

    public function getProductSizeByProductId(string $productId)
    {
        $dbResult = $this->db->select(self::TABLE_NAME_PRODUCT_SIZE,
                $columnName = 'product_id', $parameter = $productId);
        
        foreach ($dbResult as $row) {
            $productSize = new ProductSizeModel();
            $productSize->setId($row['id']);
            $productSize->setProductId($row['product_id']);
            $productSize->setSize($row['size']);

            $this->productSizeData[] = $productSize;
        }
        return $this;
    }
}
