<?php

namespace Youtube\models;

class OrdersModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_ORDERS = 'orders';
    private $orderId;
    private $cartId;
    private $paymentStatus;
    private $paymentTime;
    private $totalCost;
    private $paymentType;
    
    
    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId(string $orderId)
    {
        $this->orderId = $orderId;
    }

    public function getCartId()
    {
        return $this->cartId;
    }

    public function setCartId(string $cartId)
    {
        $this->cartId = $cartId;
    }

    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(string $paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
    }

    public function getPaymentTime()
    {
        return $this->paymentTime;
    }

    public function setPaymentTime(int $paymentTime)
    {
        $this->paymentTime = $paymentTime;
    }

    public function getTotalCost()
    {
        return $this->totalCost;
    }

    public function setTotalCost(string $totalCost)
    {
        $this->totalCost = $totalCost;
    }
    
        public function getPaymentType()
    {
        return $this->paymentType;
    }

    public function setPaymentType(string $paymentType)
    {
        $this->paymentType = $paymentType;
    }

    public function insertOrders(array $param)
    {
        $result = $this->db->insert(self::TABLE_NAME_ORDERS, $param);
        return $result;
    }

    public function selectAllByOrderId($orderId)
    {
        $result = $this->db->select(self::TABLE_NAME_ORDERS, $columnName = 'orderId', $parameter = $orderId);
        foreach ($result as $value) {
            $this->orderId = $value['orderId'];
            $this->cartId = $value['cartId'];
            $this->paymentStatus = $value['paymentStatus'];
            $this->paymentTime = $value['paymentTime'];
            $this->totalCost = $value['totalCost'];
            $this->paymentType = $value['paymentType'];
        }
        return $this;
    }

    public function selectOrderId($paymentTime)
    {
        $result = $this->db->select(self::TABLE_NAME_ORDERS, $columnName = 'cartId', $parameter = $_SESSION['cartId'],
                $rowName = 'orderId', $secondColumnName = 'paymentTime',
                $secondParameter = $paymentTime);
        foreach ($result as $value) {
            $this->orderId = $value['orderId'];
        }

        return $this->orderId;
    }
}
