<?php

namespace Youtube\models;

class ProductModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_PRODUCT = 'products';
    private $id;
    private $name;
    private $price;
    private $description;
    private $symbol;
    private $img;
    public function getId()
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(string $price)
    {
        $this->price = $price;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol)
    {
        $this->symbol = $symbol;
    }

    public function getImg()
    {
        return $this->img;
    }

    public function setImg(string $img)
    {
        $this->img = $img;
    }

    public function insertProducts($param)
    {
        $result = $this->db->insert(self::TABLE_NAME_PRODUCT, $param);
        return $result;
    }

    public function deleteProduct($parameter, $id)
    {
        $result = $this->db->delete(self::TABLE_NAME_PRODUCT, $parameter, $id);
        return $result;
    }
}
