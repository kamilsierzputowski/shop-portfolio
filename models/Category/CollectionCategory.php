<?php

namespace Youtube\models\Category;
use Youtube\libs\Database;
use Youtube\models\CategoryModel;

class CollectionCategory
{
    protected const TABLE_NAME = 'category';
    protected $categories = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getCategories(): array
    {
        return $this->categories;
    }

    public function setCategories(array $categories)
    {
        $this->categories = $categories;
    }

    public function loadAll()
    {
        $dbResult = $this->db->select(self::TABLE_NAME);
        foreach ($dbResult as $row) {
            $category = new CategoryModel();
            $category->setId($row['id']);
            $category->setName($row['Name']);
            $category->setPath($row['Path']);
            $this->categories[] = $category;
        }
        return $this;
    }
}
