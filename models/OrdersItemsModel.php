<?php
namespace Youtube\models;

class OrdersItemsModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_ORDERS_ITEMS = 'orders_items';
    private $id;
    private $itemId;
    private $itemSize;
    private $quantity;
    private $cartId;
    private $orderId;
    
    public function getId()
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getItemId()
    {
        return $this->itemId;
    }

    public function setItemId(string $itemId)
    {
        $this->itemId = $itemId;
    }
    
        public function getItemSize()
    {
        return $this->itemSize;
    }

    public function setItemSize(string $itemSize)
    {
        $this->itemSize = $itemSize;
    }
    
        public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity)
    {
        $this->quantity = $quantity;
    }
    
        public function getCartId()
    {
        return $this->cartId;
    }

    public function setCartId(string $cartId)
    {
        $this->cartId = $cartId;
    }
    
            public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId(string $orderId)
    {
        $this->orderId = $orderId;
    }

    public function insertOrdersItems(array $param)
    {
        $result = $this->db->insert(self::TABLE_NAME_ORDERS_ITEMS, $param);
        return $result;
    }
    
    public function insertIntoSelect()
    {
        $sql = "INSERT INTO orders_items (itemId, itemSize, quantity, cartId)
SELECT item_id, item_size, quantity, cartId
FROM cart_items
WHERE cartId = {$_SESSION['cartId']}";
        $result = $this->db->executeRawSql($sql);
        return $result;
        
        
    }
        public function updateOrderId()
    {

        $result = $this->db->update(self::TABLE_NAME_ORDERS_ITEMS, $column = 'orderId',
                $value = $_SESSION['orderId'], $row = 'orderId');
        return $result;
        
        

    }
}