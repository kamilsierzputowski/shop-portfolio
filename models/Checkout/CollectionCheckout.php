<?php

namespace Youtube\models\Checkout;
use Youtube\libs\Database;
use Youtube\models\CheckoutModel;

class CollectionCheckout
{
    protected const TABLE_NAME_USER_DATA = 'user_data';
    protected $userData = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getUserData(): array
    {
        return $this->userData;
    }

    public function setUserData(array $userData)
    {
        $this->userData = $userData;
    }

    public function getAllUserData()
    {

        $dbResult = $this->db->select(self::TABLE_NAME_USER_DATA,
                $columnName = 'userId', $_SESSION['cart_userId']);

        foreach ($dbResult as $row) {

            $checkout = new CheckoutModel();
            $checkout->setId($row['id']);
            $checkout->setName($row['name']);
            $checkout->setSurname($row['surname']);
            $checkout->setAddress($row['address']);
            $checkout->setCity($row['city']);
            $checkout->setZip($row['zip']);
            $checkout->setEmail($row['email']);
            $checkout->setPhoneNumber($row['phoneNumber']);
            $checkout->setUserId($row['userId']);

            $this->userData[] = $checkout;
        }
        return $this;
    }
}
