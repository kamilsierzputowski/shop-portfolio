<?php

namespace Youtube\models\OrderDetails;
use Youtube\libs\Database;
use Youtube\models\OrdersItemsModel;
use Youtube\models\ProductModel;

class CollectionOrderDetails
{
    protected const TABLE_NAME_ORDERS_ITEMS = 'orders_items';
    protected const TABLE_NAME_PRODUCT = 'products';
    protected $orderDetailsData = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getOrderDetailsData(): array
    {
        return $this->orderDetailsData;
    }

    public function setOrderDetailsData(array $orderDetailsData)
    {
        $this->orderDetailsData = $orderDetailsData;
    }

    public function getAllOrderDetailsData($param)
    {
        $dbResult = $this->db->innerJoin(self::TABLE_NAME_ORDERS_ITEMS,
                self::TABLE_NAME_PRODUCT, $table1Column_name = 'itemId',
                $table2Column_name = 'id', $conditionColumn = 'orderId',
                $conditionData = $param);

        foreach ($dbResult as $row) {
            $ordersItems = new OrdersItemsModel();
            $ordersItems->setId($row['id']);
            $ordersItems->setItemId($row['itemId']);
            $ordersItems->setItemSize($row['itemSize']);
            $ordersItems->setQuantity($row['quantity']);
            $ordersItems->setCartId($row['cartId']);
            $ordersItems->setOrderId($row['orderId']);
            $product = new ProductModel();
            $product->setId($row['id']);
            $product->setName($row['name']);
            $product->setPrice($row['price']);
            $product->setDescription($row['description']);
            $product->setSymbol($row['symbol']);
            $product->setImg($row['img']);

            $this->orderDetailsData[] = ['ordersItems' => $ordersItems,
                'product' => $product];
        }
        return $this;
    }
}
