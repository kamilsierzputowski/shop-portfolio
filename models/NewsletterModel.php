<?php

namespace Youtube\models;

class NewsletterModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_NEWSLETTER = 'newsletter';
    private $id;
    private $email;
    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function insertEmail()
    {
        $result = $this->db->insert(self::TABLE_NAME_NEWSLETTER, $_POST);
        return $result;
    }

    public function selectEmail()
    {
        $result = $this->db->select(self::TABLE_NAME_NEWSLETTER, $columnName = 'email',
                $parameter = $_POST['email'], $rowName = 'id');
        foreach ($result as $value) {
            $this->id = $value['id'];
        }
        if ($this->id === null) {
            return false;
        } else {
            return true;
        }
    }
}
