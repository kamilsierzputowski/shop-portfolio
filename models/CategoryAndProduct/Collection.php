<?php

namespace Youtube\models\CategoryAndProduct;
use Youtube\libs\Database;
use Youtube\models\ProductModel;
use Youtube\models\CategoryProductModel;

class Collection
{
    protected const TABLE_NAME_PRODUCT = 'products';
    protected const TABLE_NAME_CATEGORY_PRODUCTS = 'category_products';
    protected $productInfo = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getProductInfo(): array
    {
        return $this->productInfo;
    }

    public function setProductInfo(array $productInfo)
    {
        $this->productInfo = $productInfo;
    }

    public function getProductInfoByCategoryId($param)
    {
        $dbResult = $this->db->innerJoin(self::TABLE_NAME_PRODUCT,
                self::TABLE_NAME_CATEGORY_PRODUCTS, $table1Column_name = 'id',
                $table2Column_name = 'product_id', $conditionColumn = 'category_id',
                $conditionData = $param);

        foreach ($dbResult as $row) {
            $product = new ProductModel();
            $product->setId($row['id']);
            $product->setName($row['name']);
            $product->setPrice($row['price']);
            $product->setDescription($row['description']);
            $product->setSymbol($row['symbol']);
            $product->setImg($row['img']);
            $categoryProduct = new CategoryProductModel();
            $categoryProduct->setId($row['id']);
            $categoryProduct->setProductId($row['product_id']);
            $categoryProduct->setCategoryId($row['category_id']);

            $this->productInfo[] = ['product' => $product,
                'categoryProduct' => $categoryProduct];
        }
        return $this;
    }
}
