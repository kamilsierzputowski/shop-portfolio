<?php

namespace Youtube\models;

class CategoryModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME = 'category';
    private $id;
    private $name;
    private $path;
    public function getId()
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath(string $path)
    {
        $this->path = $path;
    }

    public function selectProductId($data)
    {
        $result = $this->db->select(self::TABLE_NAME, $columnName = 'id', $data, $rowName = 'Name');

        foreach ($result as $value) {
            $this->name = $value['Name'];
        }

        return $this;
    }
}
