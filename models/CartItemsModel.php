<?php

namespace Youtube\models;

class CartItemsModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_CART_ITEMS = 'cart_items';
    private $cartItemsId;
    private $itemId;
    private $itemSize;
    private $quantity;
    private $cartId;
    public function getCartItemsId()
    {
        return $this->cartItemsId;
    }

    public function setCartItemsId(string $cartItemsId)
    {
        $this->cartItemsId = $cartItemsId;
    }

    public function getItemId()
    {
        return $this->itemId;
    }

    public function setItemId(string $itemId)
    {
        $this->itemId = $itemId;
    }

    public function getItemSize()
    {
        return $this->itemSize;
    }

    public function setItemSize(string $itemSize)
    {
        $this->itemSize = $itemSize;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity)
    {
        $this->quantity = $quantity;
    }

    public function getCartId()
    {
        return $this->cartId;
    }

    public function setCartId(string $cartId)
    {
        $this->cartId = $cartId;
    }

    public function insertProducts($param)
    {
        $result = $this->db->insert(self::TABLE_NAME_CART_ITEMS, $param);
        var_dump($result);
        return $result;
    }

    public function deleteCartItemProduct($param, $id)
    {
        $result = $this->db->delete(self::TABLE_NAME_CART_ITEMS, $param, $id);
        return $result;
    }
}
