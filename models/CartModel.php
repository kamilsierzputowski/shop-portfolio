<?php

namespace Youtube\models;

class CartModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_CART = 'cart';
    private $cartId;
    private $userId;
    private $totalCost;
    private $cookie;
    public function getCartId()
    {
        return $this->cartId;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId(string $userId)
    {
        $this->userId = $userId;
    }

    public function getTotalCost()
    {
        return $this->totalCost;
    }

    public function setTotalCost(string $totalCost)
    {
        $this->totalCost = $totalCost;
    }

    public function getCookie()
    {
        return $this->cookie;
    }

    public function setCookie(string $cookie)
    {
        $this->cookie = $cookie;
    }

    public function insertUser(array $param)
    {
        $result = $this->db->insert(self::TABLE_NAME_USERS, $param);
        return $result;
    }

    public function createNewCart($userId)
    {
        $result = $this->db->insert(self::TABLE_NAME_CART, $param = [
            'user_id' => $userId]);
        return $result;
    }

    public function updateTotal($totalCost, $id)
    {

        $result = $this->db->update(self::TABLE_NAME_CART, $column = 'total_cost',
                $value = $totalCost, $row = 'cartId', $condition = $id);
        return $result;
    }

    public function selectCartIdForCookie()
    {

        $result = $this->db->select(self::TABLE_NAME_CART, $columnName = 'cookie',
                $parameter = 'cart', $rowName = 'cartId');

        if (empty($result)) {
            $this->db->insert(self::TABLE_NAME_CART, [
                'cookie' => $_COOKIE['cookie']]);
            $result = $this->selectCartIdForCookie($cookieValue);
        }

        foreach ($result as $value) {
            
        }

        $this->id = $value['cartId'];
        return $this->id;
    }

    public function selectCartIdForUser($userId)
    {
        $result = $this->db->select(self::TABLE_NAME_CART, $columnName = 'user_id',
                $parameter = $userId, $rowName = 'cartId');
        foreach ($result as $value) {
            
        }

        $this->id = $value['cartId'];
        return $this->id;
    }

    public function totalCost()
    {
        $result = $this->db->select(self::TABLE_NAME_CART, $columnName = 'cartId', $_SESSION['cartId']);
        foreach ($result as $value) {
            
        }

        $this->totalCost = $value['total_cost'];
        return $this->totalCost;
    }
}
