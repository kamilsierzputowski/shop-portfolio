<?php

namespace Youtube\models;

class CategoryProductModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_CATEGORY_PRODUCT = 'category_products';
    private $id;
    private $productId;
    private $categoryId;
    public function getId()
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getProductId()
    {
        return $this->productId;
    }

    public function setProductId(string $productId)
    {
        $this->productId = $productId;
    }

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function setCategoryId(string $categoryId)
    {
        $this->categoryId = $categoryId;
    }

    public function insertProducts($param)
    {
        $result = $this->db->insert(self::TABLE_NAME_CATEGORY_PRODUCT, $param);
        return $result;
    }
}
