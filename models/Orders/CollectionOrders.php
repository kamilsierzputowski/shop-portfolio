<?php

namespace Youtube\models\Orders;
use Youtube\libs\Database;
use Youtube\models\OrdersModel;

class CollectionOrders
{
    protected const TABLE_NAME_ORDERS = 'orders';
    protected $ordersData = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getOrdersData(): array
    {
        return $this->ordersData;
    }

    public function setOrdersData(array $ordersData)
    {
        $this->ordersData = $ordersData;
    }

    public function getAllOrdersData($cartId = null)
    {
        if ($cartId === null) {
            $dbResult = $this->db->select(self::TABLE_NAME_ORDERS);
        } else {
            $dbResult = $this->db->select(self::TABLE_NAME_ORDERS,
                    $columnName = 'cartId', $parameter = $cartId);
        }
        
        foreach ($dbResult as $row) {

            $order = new OrdersModel();
            $order->setOrderId($row['orderId']);
            $order->setCartId($row['cartId']);
            $order->setPaymentStatus($row['paymentStatus']);
            $order->setPaymentTime($row['paymentTime']);
            $order->setTotalCost($row['totalCost']);
            $order->setPaymentType($row['paymentType']);

            $this->ordersData[] = $order;
        }
        return $this;
    }
}
