<?php

namespace Youtube\models;

class CheckoutModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_USER_DATA = 'user_data';
    private $id;
    private $name;
    private $surname;
    private $address;
    private $city;
    private $zip;
    private $email;
    private $phoneNumber;
    private $userId;
    public function getId()
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname(string $surname)
    {
        $this->surname = $surname;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity(string $city)
    {
        $this->city = $city;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function setZip(string $zip)
    {
        $this->zip = $zip;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId(string $userId)
    {
        $this->userId = $userId;
    }

    public function selectUserData($parameter = null)
    {
        if ($parameter === null) {
            $result = $this->db->select(self::TABLE_NAME_USER_DATA);
        } else {
            $result = $this->db->select(self::TABLE_NAME_USER_DATA,
                    $columnName = 'userId', $parameter);
        }

        foreach ($result as $value) {
            $this->id = $value['id'];
            $this->name = $value['name'];
            $this->surname = $value['surname'];
            $this->address = $value['address'];
            $this->city = $value['city'];
            $this->zip = $value['zip'];
            $this->email = $value['email'];
            $this->phoneNumber = $value['phoneNumber'];
            $this->userId = $value['userId'];
        }
        return $this;
    }

    public function deleteUserData()
    {
        $result = $this->db->delete(self::TABLE_NAME_USER_DATA, 
                $parameter = 'userId', $_SESSION['cart_userId']);
        return $result;
    }

    public function insertUserData(array $param)
    {
        $result = $this->db->insert(self::TABLE_NAME_USER_DATA, $param);
        return $result;
    }
}
