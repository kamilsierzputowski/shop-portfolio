<?php

namespace Youtube\models;

use Youtube\libs\Session;

class UserModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_USERS = 'users';
    private $id;
    private $login;
    private $password;
    public function getId()
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $this->login = $login;
    }

    public function getPassword()
    {
        return $this->lastname;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function insertUser(array $param)
    {
        $result = $this->db->insert(self::TABLE_NAME_USERS, $param);
        return $result;
    }

    public function selectUserId($login, $password)
    {
        $result = $this->db->select(self::TABLE_NAME_USERS, $columnName = 'login',
                $parameter = $login, $rowName = 'id', $columnName2 = 'password',
                $parameter2 = $password);
        foreach ($result as $value) {
            
        }

        $this->id = $value['id'];
        return $this->id;
    }

    public function userExist()
    {
        $userModel = new \Youtube\models\UserModel();
        $result = $this->db->select(self::TABLE_NAME_USERS, $columnName = 'login',
                $_POST['login'], $rowName = 'id');
        foreach ($result as $value) {
            $userModel->setId($value['id']);
        }

        $userId = $userModel->getId();
        if (isset($userId)) {
            return true;
        } else {
            return false;
        }
    }

    public function loginRun()
    {
        $userModel = new \Youtube\models\UserModel();
        $pass = md5($_POST['password']);

        $sth = $this->db->select(self::TABLE_NAME_USERS, $columnName = 'login', $parameter = $_POST['login'],
                $rowName = 'id', $columnName2 = 'password', $parameter2 = $pass);
        foreach ($sth as $value) {
            $userModel->setId($value['id']);
        }

        $userId = $userModel->getId();

        if (isset($userId)) {

            Session::init();
            Session::set('loggedIn', true);
            Session::set('login', $_POST['login']);
            Session::set('password', $_POST['password']);
            echo json_encode(['status' => '1']);
        } else {
            echo json_encode(['status' => '0']);
        }
    }
}
