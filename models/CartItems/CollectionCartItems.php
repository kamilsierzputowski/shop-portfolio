<?php

namespace Youtube\models\CartItems;
use Youtube\models\ProductModel;
use Youtube\models\CartItemsModel;
use Youtube\libs\Database;

class CollectionCartItems
{
    protected const TABLE_NAME_CART_ITEMS = 'cart_items';
    protected const TABLE_NAME_PRODUCT = 'products';
    protected $cartdata = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getCartData(): array
    {
        return $this->cartdata;
    }

    public function setCartData(array $cartdata)
    {
        $this->cartdata = $cartdata;
    }

    public function getCartDataByCartId($cartId)
    {
        $dbResult = $this->db->innerJoin(self::TABLE_NAME_CART_ITEMS,
                self::TABLE_NAME_PRODUCT, $FirstTableColumnName = 'item_id',
                $SecondTableColumnName = 'id', $conditionColumn = 'cartId',
                $conditionData = $cartId);

        foreach ($dbResult as $row) {

            $product = new ProductModel();
            $product->setId($row['id']);
            $product->setName($row['name']);
            $product->setPrice($row['price']);
            $product->setDescription($row['description']);
            $product->setSymbol($row['symbol']);
            $product->setImg($row['img']);
            $cartItems = new CartItemsModel();
            $cartItems->setCartItemsId($row['cart_itemsId']);
            $cartItems->setItemId($row['item_id']);
            $cartItems->setItemSize($row['item_size']);
            $cartItems->setQuantity($row['quantity']);
            $cartItems->setCartId($row['cartId']);

            $this->cartdata[] = ['product' => $product,
                'cartItems' => $cartItems];
        }
        return $this;
    }
}
