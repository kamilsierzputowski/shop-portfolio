<?php

namespace Youtube\models;

class ProductSizeModel extends \Youtube\libs\Model
{
    protected const TABLE_NAME_PRODUCT_SIZE = 'product_size';
    private $id;
    private $productId;
    private $size;
    public function getId()
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getProductId()
    {
        return $this->productId;
    }

    public function setProductId(string $productId)
    {
        $this->productId = $productId;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize(string $size)
    {
        $this->size = $size;
    }
}
