<?php

namespace Youtube\models\OrdersItems;
use Youtube\libs\Database;
use Youtube\models\OrdersItemsModel;

class CollectionOrdersItems
{
    protected const TABLE_NAME_ORDERS_ITEMS = 'orders_items';
    protected $ordersItemsData = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getOrdersItemsData(): array
    {
        return $this->ordersItemsData;
    }

    public function setOrdersItemsData(array $ordersItemsData)
    {
        $this->ordersItemsData = $ordersItemsData;
    }

    public function getAllOrdersItemsData($orderId = null)
    {
        if ($orderId === null) {
            $dbResult = $this->db->select(self::TABLE_NAME_ORDERS_ITEMS);
        } else {
            $dbResult = $this->db->select(self::TABLE_NAME_ORDERS_ITEMS,
                    $columnName = 'orderId', $parameter = $orderId);
        }
        
        foreach ($dbResult as $row) {

            $ordersItems = new OrdersItemsModel();
            $ordersItems->setId($row['id']);
            $ordersItems->setItemId($row['itemId']);
            $ordersItems->setItemSize($row['itemSize']);
            $ordersItems->setQuantity($row['quantity']);
            $ordersItems->setCartId($row['cartId']);
            $ordersItems->setOrderId($row['orderId']);

            $this->ordersItemsData[] = $ordersItems;
        }
        return $this;
    }
}
