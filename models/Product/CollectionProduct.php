<?php

namespace Youtube\models\Product;
use Youtube\libs\Database;
use Youtube\models\ProductModel;

class CollectionProduct
{
    protected const TABLE_NAME_PRODUCT = 'products';
    protected $productData = [];
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getProductData(): array
    {
        return $this->productData;
    }

    public function setProductData(array $productData)
    {
        $this->productData = $productData;
    }

    public function getAllProductData(string $productId = null)
    {
        if ($productId === null) {
            $dbResult = $this->db->select(self::TABLE_NAME_PRODUCT);
        } else {
            $dbResult = $this->db->select(self::TABLE_NAME_PRODUCT,
                    $columnName = 'id', $parameter = $productId);
        }

        foreach ($dbResult as $row) {

            $product = new ProductModel();
            $product->setId($row['id']);
            $product->setName($row['name']);
            $product->setPrice($row['price']);
            $product->setDescription($row['description']);
            $product->setSymbol($row['symbol']);
            $product->setImg($row['img']);

            $this->productData[] = $product;
        }
        return $this;
    }
}
