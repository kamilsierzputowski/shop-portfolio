$(document).ready(function () {
    $('input[required]').addClass('highlighted');

    $('body').on('click', 'button.switch', function () {
        $('body').toggleClass('backgroundsetting');
    });

    $('.col-6').on('click', 'a.info-link', function (event) {
        event.preventDefault();
        $(this).closest('.col-6').find('.more-info').fadeToggle('slow');
    });

    $('#bycard').on('change', function () {
        $('#credit-card-row input').prop('required', true);
        if ($(this).is(':checked')) {
            $('#credit-card-row').show();
            $('#checkout-button').show();
            $('#info-byCOD').hide();

        } else {
            $('#credit-card-row').hide();
            $('#checkout-button').hide();
            $('#credit-card-row input').attr('required');
        }
    });

    $('#bycard').trigger('change');

    $('#byCOD').on('change', function () {
        if ($(this).is(':checked')) {
            $('#info-byCOD').show();
            $('#checkout-button').show();
            $('#credit-card-row').hide();
            $('#credit-card-row input').removeAttr('required');
        } else {
            $('#info-byCOD').hide();
            $('#checkout-button').hide();
            $('#credit-card-row input').attr('required');
        }
    });

    $("img#zoom").hover(function () {
        $(this).addClass('transition');
        $(this).addClass('transition');
    }, function () {
        $(this).removeClass('transition');
    });

    $('.conteiner-cart').addClass('conteiner-product');

    $('#addtocart').on('submit', function (e) {
        console.log($('#addtocart').serialize());
        e.preventDefault();

        $.ajax({
            url: '/CartItems/index',
            type: 'POST',
            cache: false,
            data: $('#addtocart').serialize(),
            dataType: 'json',
            complete: function (response) {
                console.log(response);
                $('.product-message').html('added to cart');
                $('.product-message').addClass('form-success');
                $('.product-message').css('margin-top', '15px');
            }
        });
    });

    $('#newsletter-form').on('submit', function (e) {
        console.log($('#newsletter-form').serialize());
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/Newsletter/index',
            cache: false,
            data: $('#newsletter-form').serialize(),
            dataType: "json",
            success: function (data) {
                console.log(JSON.parse(JSON.stringify(data.status)));
                $('#newsletter-form-row').css("display", "none");
                if (JSON.parse(JSON.stringify(data.status == 'ok')))
                {
                    $('#newsletter-form-row').css({"display": "flex",
                        'color': 'green'});
                    $('.newsletter-message').html('<center>Thank you!<br> \n\
           Check out your email and enjoy shopping</center>');
                } else {
                    $('#newsletter-form-row').css({"display": "flex",
                        'color': 'red'});
                    $('.newsletter-message').html('<center>This email is sign in in our newsletter</center>');
                }
            }
        });

    });

    $('#signUp').on('click', function (e) {
        e.preventDefault();
        $('#login-form').css('display', 'none');
        $('#register').css('display', 'inline');
    });

    $('#login-form').on('submit', function (e) {
        console.log($('#login-form')[0].checkValidity());
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/Login/run',
            cache: false,
            data: $('#login-form').serialize(),
            dataType: "json",
            success: function (data) {
                console.log(JSON.parse(JSON.stringify(data.status)));
                if (JSON.parse(JSON.stringify(data.status)) === '1') {
                    window.location = "/Dashboard";
                } else {
                    $('.login-msg').html('incorrect password or login');
                    $('.login-msg').addClass('form-error', 2000);
                }
            }
        });
    });


    $('form#register').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: 'post',
            url: '/AddUser/index',
            cache: false,
            data: $('#register').serialize(),
            dataType: 'json',
            success: function (data) {
                console.log(JSON.parse(JSON.stringify(data.status)));
                if (JSON.parse(JSON.stringify(data.status === 'inserted')))
                {
                    $('.register-msg').html('Great! now you can log in');
                    $('.register-msg').addClass('form-success');

                } else {
                    $('.register-msg').html('This login is occupied, try another one');
                    $('.register-msg').addClass('form-error');
                }
            }
        });
    });
});
