<?php

namespace Youtube\controllers;

use Youtube\libs\Session;
use Youtube\models\Orders\CollectionOrders;

class Orders extends \Youtube\libs\Controller
{
    public function index()
    {
        if (Session::get('loggedIn') === null) {
            header('location: ../Login');
        }

        $orders = new CollectionOrders();
        $ordersData = $orders->getAllOrdersData($_SESSION['cartId'])->getOrdersData();

        $this->view->render('orders/view', $ordersData);
    }
}
