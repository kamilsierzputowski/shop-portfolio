<?php

namespace Youtube\controllers;

use Youtube\models\ProductModel;
use Youtube\models\Product\CollectionProduct;

class Index extends \Youtube\libs\Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    { 
        $collection = new CollectionProduct();
        $selectedProducts = $collection->getAllProductData()->getProductData();
        
        $this->view->render('index/view', $selectedProducts);
    }

    public function delete()
    {
        foreach ($_POST as $key => $value) {
            $delete = new ProductModel();
            $delete->deleteProduct($key, $value);
        }
        \header('location: ../Index');
    }
}
