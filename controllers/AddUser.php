<?php

namespace Youtube\controllers;

use Youtube\models\UserModel;

class AddUser extends \Youtube\libs\Controller
{
    public function index()
    {
        if (!empty($_POST)) {
            $user = new UserModel();
            $userExist = $user->userExist(); 
            if ($userExist === false) {
                $_POST['password'] = md5($_POST['password']);
                
                $user->insertUser($_POST);
                ob_clean();
                echo json_encode(['status' => 'inserted']);
                die();
            } else {
                ob_clean();
                echo json_encode(['status' => 'notinserted']);
                die();
            }
        }
    }
}
