<?php

namespace Youtube\controllers;

use Youtube\models\Product\CollectionProduct;
use Youtube\models\Category\CollectionCategory;
use Youtube\models\CategoryProductModel;

class CategoryProduct extends \Youtube\libs\Controller
{
    public function index()
    {
        $sortProducts = new CollectionProduct();
        $products = $sortProducts->getAllProductData()->getProductData();

        $sortCategories = new CollectionCategory();
        $catalog = $sortCategories->loadAll()->getCategories();

        $this->view->render('categoryProduct/view', [
            'products' => $products,
            'catalog' => $catalog
        ]);
    }

    public function run()
    {
        $this->view->render('categoryProduct/view', []);
        if (isset($_POST['submit'])) {
            if (!empty($_POST['product_id']) && !empty($_POST['category_id'])) {
                $categoryAndProduct = new CategoryProductModel();
                $categoryAndProduct->insertProducts($_POST);
                header('location: ../Category');
            } else {
                header('location: ../CategoryProduct');
            }
        }
    }
}
