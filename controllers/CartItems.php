<?php

namespace Youtube\controllers;

use Youtube\models\CartModel;
use Youtube\models\CartItemsModel;
use Youtube\libs\Session;

class CartItems extends \Youtube\libs\Controller
{
    public function index()
    {
        $logged = Session::get('loggedIn');

        if ($logged === true) {
            $cartItems = new CartItemsModel();    
            $cartItems->insertProducts($_POST);
        } else {
            setcookie(cookieName, cookieValue, time() + parent::COOKIE_LIFETIME);        
            $cart = new CartModel(); 
            $selectCartId = $cart->selectCartIdForCookie($_COOKIE['cookie']);
            
            $_SESSION['cartId'] = $selectCartId;
            
            $this->model->insertProducts([
                'item_id' => $_POST['item_id'],
                'item_size' => $_POST['item_size'],
                'quantity' => $_POST['quantity'],
                'cartId' => $_SESSION['cartId'],
                'submit' => $_POST['submit']
            ]);
        }
    }
}
