<?php

namespace Youtube\controllers;

use Youtube\models\OrdersModel;
use Youtube\models\OrderDetails\CollectionOrderDetails;
use Youtube\models\CheckoutModel;

class OrderDetails extends \Youtube\libs\Controller
{
    public function index()
    {
        $url = explode('/', $_GET['url']);

        $orderId = $url[2];

        $order = new OrdersModel();
        $orderData = $order->selectAllByOrderId($orderId);

        $user = new CheckoutModel();
        $userData = $user->selectUserData($_SESSION['cart_userId']);

        $collectionOrderDetails = new CollectionOrderDetails();
        $orderDetailsData = $collectionOrderDetails->getAllOrderDetailsData($orderId)->getOrderDetailsData();

        $this->view->render('orderDetails/view', [
            'orderDetailsData' => $orderDetailsData,
            'order' => $orderData,
            'user' => $userData
        ]);
    }
}
