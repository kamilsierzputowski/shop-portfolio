<?php

namespace Youtube\controllers;

use Youtube\models\CartModel;
use Youtube\models\CartItemsModel;
use Youtube\models\CartItems\CollectionCartItems;
use Youtube\libs\Session;

class Cart extends \Youtube\libs\Controller
{
    public function index()
    {
        $logged = Session::get('loggedIn');
        
        $cart = new CartModel();
        
        if ($logged === null) {
            setcookie(cookieName, cookieValue, time() + parent::COOKIE_LIFETIME);
            $selectCartId = $cart->selectCartIdForCookie();
            
            $_SESSION['cartId'] = $selectCartId;
        }

        $cartItems = new CollectionCartItems();
        $itemsData = $cartItems->getCartDataByCartId($_SESSION['cartId'])->getCartData();
        
        $totalCost = 0;
        
        foreach ($itemsData as $value) {
            $price = $value['product']->getPrice();
            $quantity = $value['cartItems']->getQuantity();
            $productValue = $price * $quantity;
            $totalCost += $productValue;
        }
        
        $_SESSION['totalCost'] = $totalCost;

        $update = $cart->updateTotal($totalCost, $_SESSION['cartId']);
        $this->view->render('cart/view', [
            'itemsData' => $itemsData,
            'total_value' => $totalCost
        ]);
    }

    public function delete()
    {
        foreach ($_POST as $key => $value) {
            $cartItems = new CartItemsModel();
            $cartItems->deleteCartItemProduct('cart_itemsId', $value);
        }
        header('location: ../Cart');
    }
}
