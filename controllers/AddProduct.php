<?php

namespace Youtube\controllers;

use Youtube\models\ProductModel;

class AddProduct extends \Youtube\libs\Controller
{
    public function index()
    {
        if (isset($_POST['submit'])) {
            if (!empty($_POST['name']) && !empty($_POST['price']) &&
                    !empty($_POST['description']) && !empty($_POST['symbol']) &&
                    !empty($_POST['img'])) {
                $products = new ProductModel();
                $products->insertProducts($_POST);
                header('location: ../Index');
            } else {
                header('location: ../AddProduct');
            }
        }
        $this->view->render('addProduct/view', []);
    }
}
