<?php

namespace Youtube\controllers;

use Youtube\models\Category\CollectionCategory;

class Category extends \Youtube\libs\Controller
{
    public function index()
    {
        $category = new CollectionCategory();   
        $categories = $category->loadAll()->getCategories();
        
        $this->view->render('category/view', $categories);
    }
}
