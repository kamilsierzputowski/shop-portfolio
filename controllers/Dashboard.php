<?php

namespace Youtube\controllers;

use Youtube\models\UserModel;
use Youtube\models\CartModel;
use Youtube\libs\Session;

class Dashboard extends \Youtube\libs\Controller
{
    public function __construct()
    {
        parent::__construct();
        Session::init();
        unset($_SESSION['cartId']);
        $logged = Session::get('loggedIn');
        if ($logged === false) {
            Session::destroy();
            header('location: ../Login');
            exit;
        }
    }

    public function index()
    {
        $login = $_SESSION['login'];

        $password = md5($_SESSION['password']);

        $cart = new CartModel();
        $addUser = new UserModel();
        $userId = $addUser->selectUserId($login, $password);

        Session::set('cart_userId', $userId);
        $connectCartId = $cart->selectCartIdForUser($_SESSION['cart_userId']);

        Session::set('cartId', $connectCartId);
        if (!isset($_SESSION['cartId'])) {
            $cart->createNewCart($_SESSION['cart_userId']);
            $connectCartId2 = $cart->selectCartIdForUser($_SESSION['cart_userId']);

            $_SESSION['cartId'] = $connectCartId2;
        }
        $this->view->render('dashboard/view', []);
        \header('location: ../Category');
    }

    public function logout()
    {
        Session::destroy();
        header('location: ../Login');
    }
}
