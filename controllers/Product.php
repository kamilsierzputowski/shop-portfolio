<?php

namespace Youtube\controllers;

use Youtube\models\Product\CollectionProduct;
use Youtube\models\ProductSize\CollectionProductSize;

class Product extends \Youtube\libs\Controller
{
    public function index()
    {
        $url = explode('/', $_GET['url']);

        $productId = $url[4];
        
        $productsCollection = new CollectionProduct();
        $productsData = $productsCollection->getAllProductData($productId)->getProductData();

        $productSizeCollection = new CollectionProductSize();
        $sizeData = $productSizeCollection->getProductSizeByProductId($productId)->getProductSizeData();

        $this->view->render('product/view', ['productsData' => $productsData,
            'sizeData' => $sizeData
        ]);
    }
}
