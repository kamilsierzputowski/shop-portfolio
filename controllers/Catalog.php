<?php

namespace Youtube\controllers;

use Youtube\models\CategoryModel;
use Youtube\models\CategoryAndProduct\Collection;

class Catalog extends \Youtube\libs\Controller
{
    public function index()
    {
        $url = explode('/', $_GET['url']);

        $catalog = $url[3];
        
        $category = new CategoryModel();
        $categoryData = $category->selectProductId($catalog)->getName();

        $categoryAndProductCollection = new Collection();
        $categoryAndProductCollection->getProductInfoByCategoryId($catalog)->getProductInfo();
        $this->view->render('catalog/view', ['category' => $categoryData,
            'collection' => $categoryAndProductCollection
        ]);
    }
}
