<?php

namespace Youtube\controllers;

use Youtube\models\CheckoutModel;
use Youtube\models\OrdersModel;
use Youtube\models\OrdersItemsModel;
use Youtube\models\CartItemsModel;

class Checkout extends \Youtube\libs\Controller
{
    public function index()
    {
        if (isset($_POST['submit'])) {

            $orders = new OrdersModel();
            $orders->insertOrders([
                'cartId' => $_SESSION['cartId'],
                'paymentStatus' => $_POST['paymentStatus'],
                'paymentTime' => $_POST['paymentTime'],
                'totalCost' => $_SESSION['totalCost'],
                'paymentType' => $_POST['paymentType'],
                'submit' => $_POST['submit']
            ]);

            $orderItems = new OrdersItemsModel();
            $orderItems->insertIntoSelect();
            $orderId = $orders->selectOrderId($_POST['paymentTime']);

            unset($_SESSION['orderId']);
            $_SESSION['orderId'] = $orderId;

            $orderItems->updateOrderId();
            unset($_SESSION['orderId']);
            $cartItems = new CartItemsModel();
            $cartItems->deleteCartItemProduct($param = 'cartId', $_SESSION['cartId']);

            $userData = new CheckoutModel();
            $userId = $userData->selectUserData($_SESSION['cart_userId'])->getUserId();

            if (isset($userId)) {
                $deleteOldData = $userData->deleteUserData();
            }
            unset($_SESSION['totalCost']);
            unset($_POST['paymentTime']);
            unset($_POST['paymentType']);
            unset($_POST['paymentStatus']);
            $userData->insertUserData($_POST);
            \header('location: ../Orders');
        }
        $this->view->render('checkout/view', []);
    }
}
