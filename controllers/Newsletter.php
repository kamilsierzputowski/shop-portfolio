<?php

namespace Youtube\controllers;

use Youtube\models\NewsletterModel;

class Newsletter extends \Youtube\libs\Controller
{
    public function index()
    {
        if (isset($_POST['email'])) {
            $newsletter = new NewsletterModel();
            //checking if provided email exists in database(false === not exist)
            $selectEmail = $newsletter->selectEmail();

            if ($selectEmail === false) {
                $newsletter->insertEmail();
                echo json_encode(['status' => 'ok']);
                require 'mailer.php';
            } else {
                echo json_encode(['status' => 'error']);
            }
        }
        return false;
    }
}
