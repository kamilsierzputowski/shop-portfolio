<?php

namespace Youtube\controllers;

use Youtube\models\UserModel;

class Login extends \Youtube\libs\Controller
{
    public function index()
    {
        $this->view->render('login/view', []);
    }

    public function run()
    {
        if (!empty($_POST['login'])) {
            $userModel = new UserModel();
            $userModel->loginRun();
        } else {
            return false;
        }
    }
}
