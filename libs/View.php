<?php

namespace Youtube\libs;

class View
{
    public function render($name, array $data, $empty = false)
    {
        if ($empty == true) {
            require 'views/header.php';
            require 'views/' . $name . '.php';
        } else {
            require 'views/header.php';
            require 'views/' . $name . '.php';
            require 'views/footer.php';
        }
    }
}
