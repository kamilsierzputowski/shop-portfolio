<?php

namespace Youtube\libs;

class Controller
{
    public const COOKIE_LIFETIME = 86000;
    function __construct()
    {
        $this->view = new View();
    }

    public function loadModel($name)
    {
        $path = 'models/' . $name . 'Model.php';
        if (file_exists($path)) {
            require 'models/' . $name . 'Model.php';

            $modelName = $name . 'Model';
            $controllerPath = '\Youtube\models\\' . $modelName;

            $this->model = new $controllerPath;
        }
    }
}
