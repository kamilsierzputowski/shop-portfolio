<?php

namespace Youtube\libs;
use PDO;

class Database extends PDO
{
    public function __construct()
    {
        parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME,
                DB_USER, DB_PASS);
    }

    public function executeRawSql($sql)
    {
        $sth = $this->prepare($sql);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function select($table, $columnName = null, $parameter = null,
            $rowName = null, $columnName2 = null, $parameter2 = null)
    {
        if ($columnName && $parameter && $rowName && $columnName2 && $parameter2) {
            $sth = $this->prepare("SELECT {$rowName} FROM {$table} WHERE
            {$columnName} = '{$parameter}' AND {$columnName2} = '{$parameter2}'");
        } elseif ($columnName && $parameter && $rowName) {
            $sth = $this->prepare("SELECT {$rowName} FROM {$table} WHERE {$columnName} = '{$parameter}'");
        } elseif ($columnName && $parameter) {
            $sth = $this->prepare("SELECT * FROM {$table} WHERE {$columnName} = '{$parameter}'");
        } else {
            $sth = $this->prepare("SELECT * FROM {$table}");
        }
        $sth->execute();
        
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insert($table, $param = [])
    {
        $columns = $this->prepareColumns($param);
        $values = $this->prepareValues($param);
        $sth = $this->prepare("INSERT INTO {$table} ({$columns}) VALUES ({$values})");
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($table, $parameter, $id)
    {
        $sth = $this->prepare("DELETE FROM {$table} WHERE {$parameter} = {$id}");
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function update($table, $column, $value, $row, $condition = null)
    {
        if ($column && $value && $row && $condition) {
            $sth = $this->prepare("UPDATE {$table} SET {$column} = '{$value}'
        WHERE {$row} = {$condition}");
        } else {
            $sth = $this->prepare("UPDATE {$table} SET {$column} = '{$value}'
        WHERE {$row} IS NULL");
        }
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function prepareColumns($parameter = [])
    {
        unset($parameter['submit']);
        $columnString = '';
        foreach ($parameter as $key => $value) {
            $columnString .= ', ' . $key;
        }
        return trim(ltrim($columnString, ', '), ', ');
    }

    public function prepareValues($parameter = [])
    {
        unset($parameter['submit']);

        $valueString = '';
        foreach ($parameter as $key => $value) {
            $valueString .= ', ' . "'" . $value . "'";
        }
        return ltrim($valueString, ', ');
    }

    public function innerJoin($table1, $table2, $table1Column_name,
            $table2Column_name, $conditionColumn, $conditionData)
    {
        $sth = $this->prepare("SELECT *
        FROM {$table1}
        INNER JOIN {$table2}
        ON {$table1}.{$table1Column_name} = {$table2}.{$table2Column_name}
        WHERE {$conditionColumn} = {$conditionData}");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
}
