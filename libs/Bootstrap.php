<?php

namespace Youtube\libs;
use Youtube\controllers\Index;
use Youtube\controllers\Erroor;

class Bootstrap
{
    function __construct()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');

        $url = explode('/', $url);

        if (empty($url[0])) {
            require 'controllers/Index.php';
            $controller = new Index();
            $controller->index();
            return false;
        }

        $file = 'controllers/' . $url[0] . '.php';

        if (file_exists($file)) {
            require $file;
        } else {
            require 'controllers/Erroor.php';
            $controller = new Erroor();
            return false;
        }
        function generateclass($class)
        {
            return new $class;
        }

        $controllerPath = '\Youtube\controllers\\' . $url[0];
        $controller = new $controllerPath;

        $controller->loadModel($url[0]);

        if (isset($url[2])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]);
            } else {
                header('Location: ../');
            }
        } else {
            if (isset($url[1])) {
                $controller->{$url[1]}();
                return false;
            } else {
                $controller->index();
            }
        }
    }
}
