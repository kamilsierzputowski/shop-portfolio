<style>
    <link rel ="stylesheet" href ="<?= URL; ?>public/css/default.css" />
</style>

<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
    <button class="navbar-toggler" type="button" data-toggle="collapse" 
            data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" 
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>    
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">        
        <div class="navbar-nav">
            <?php foreach ($data as $row): ?>
                <a class="nav-item nav-link" href="<?=
                'Catalog/index' . '/id/' .
                $row->getId()
                ?>"><?= $row->getName() ?></a>
               <?php endforeach; ?>
        </div>       
    </div>   
</nav><br>

<center>
    <div id="carusel-images" class="conteiner">
        <div id="carouselExampleControls" class="carousel carousel-dark slide" data-bs-ride="carousel">      
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?= URL; ?>/public/images/11.jpg" class="d-block w-30" style="height:400;">
                </div> 
                <div class="carousel-item">
                    <img src="<?= URL; ?>/public/images/21.jpg" class="d-block w-30" style="height:400;">
                </div>
                <div class="carousel-item">
                    <img src="<?= URL; ?>/public/images/26.jpg" class="d-block w-30" style="height:400;">
                </div>
                <div class="carousel-item">
                    <img src="<?= URL; ?>/public/images/27.jpg" class="d-block w-30" style="height:400;">
                </div>
                <div class="carousel-item">
                    <img src="<?= URL; ?>/public/images/30.jpg" class="d-block w-30" style="height:400;">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button> 
        </div>
    </div>
</center>
