<style>
    <link rel ="stylesheet" href ="<?= URL ?>public/css/default.css" />
</style>

<div class="conteiner-catalog">
    <div id="row-width" class="row">
        <center><h1 id="katalogs_product"><?= $data['category'] ?></h1></center><br><hr>
    </div>  
    <div id="row-width" class="row">
        <?php foreach ($data['collection']->getProductInfo() as $value): ?>   
            <div class="col-6">
                <center>
                    <a id="katalogs_product" 
                       href="<?= URL . 'Product/index' . '/id/' .
                       $value['categoryProduct']->getCategoryId() . '/' .
                       $value['categoryProduct']->getProductId()?>"> 
                        <img id="catalog-photo" src="<?= URL; ?>/public/images/<?= $value['product']->getImg() ?>"
                             alt="alt" style="width:400px;height:300px;"/><br>
                        <?= $value['product']->getName() ?><br>
                        <?= $value['product']->getPrice() . "$" ?></a><br>    
                    <div>
                        <a href="#" class="info-link"><i>More info</i></a>
                    </div>                    
                    <div class="more-info">
                        <p><?= $value['product']->getDescription() ?></p>
                    </div>            
                </center>
            </div>    
        <?php endforeach ?> 
    </div>   
</div>