<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    <link rel ="stylesheet" href ="<?= URL; ?>public/css/default.css" />   
</style>

<div class="container">
    <div class="row">
        <div class="col">
            <a class="btn-primary" href="<?= URL . 'CategoryProduct' ?>" role="button">Enter to sort products</a>
        </div>
        <div class="col">
            <a class="btn-primary" href="<?= URL . 'AddProduct' ?>" role="button">Enter to insert product</a>
        </div>
    </div>
</div>

<form action="Index/delete" method="post">
    <table id="avaiable_prod">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Symbol</th>
            <th>Img</th>
        </tr>
        <?php foreach ($data as $row): ?>
            <tr>
                <td><button class="btn" type="submit" name="id" value="<?= $row->getId() ?>">
                        <i class="fa fa-trash"></i></button><?= " " . $row->getId() ?></td>
                <td><?= $row->getName() ?></td>
                <td><?= $row->getPrice() ?></td>
                <td><?= $row->getDescription() ?></td>
                <td><?= $row->getSymbol() ?></td>
                <td><?= $row->getImg() ?></td>
            </tr>
        <?php endforeach; ?>
    </table><br>
</form>
