<style>
    <link rel ="stylesheet" href ="<?= URL; ?>public/css/default.css" />
</style>

<h4 class="mb-3" ><?=
    'Welcome ' . $data['user']->getName() .
    ', this is Your order from ' . date('m/d/Y', $data['order']->getPaymentTime())
    ?>
</h4>
<div class="conteiner-lg">
    <div class="row">
        <div id="orderDetailsBorder" class="col-4">
            <h2>Shipping data</h2>
            <?= $data['user']->getName() . ' ' . $data['user']->getSurname() ?><br>
            <?= $data['user']->getAddress() ?><br>
            <?= $data['user']->getCity() . ' ' . $data['user']->getZip() ?><br>
            Email: <?= $data['user']->getEmail() ?><br>
            Phone number: <?= $data['user']->getPhoneNumber() ?><br>
        </div>
    </div>
    <div class="row">
        <?php foreach ($data['orderDetailsData'] as $value): ?> 
            <div class="col-4">
                <img src="<?= URL; ?>/public/images/<?= $value['product']->getImg() ?>" alt="alt" style="width:350px;height:250"/><br>
                <a id="katalogs_product">Model: <?= $value['product']->getName() ?><br>
                    Price: <?= $value['product']->getPrice() ?><br>
                    Symbol: <?= $value['product']->getSymbol() ?><br>
                    Size: <?= $value['ordersItems']->getItemSize() ?><br>
                    Quantity: <?= $value['ordersItems']->getQuantity() ?></a>     
            </div>
        <?php endforeach ?>
    </div>
    <div id="sumConteiner" class="row"> 
        <div class="col">
            <h2 id="cartH2">Total cost: <?= $data['order']->getTotalCost() . "$" ?></h2>
        </div>
    </div> 
</div>
