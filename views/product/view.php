<style>
    <link rel ="stylesheet" href ="<?= URL; ?>public/css/default.css" />
</style>

<div id="conteiner-product" class="conteiner">
    <div class="row">
        <?php foreach ($data['productsData'] as $row): ?>
            <div class="col-5">    
                <p class="s"> <?= $row->getName() ?></p><br>
                <img id="zoom" src="<?= URL; ?>/public/images/<?= $row->getImg() ?>" alt="alt" style="width:600px;height:450px;" data-zoom-image="<?= URL; ?>/public/images/<?= $row->getImg() ?>"/><br>
            </div>
            <div class="col" id="productdescription"> 
                <div class="col-md-12">   
                    Price: <?= $row->getPrice() ?>$
                </div><br>
                <div class="col-md-12">     
                    Description: <?= $row->getDescription() . "." ?>
                </div><br>
                <div class="col-md-12">   
                    Symbol: <?= $row->getSymbol() . "." ?>
                </div><br>
            <?php endforeach ?>   
            <div class="col-md-12">   
                <form id="addtocart" action="<?= URL; ?>CartItems/index" method="POST">
                    Avaiable sizes:
                    <?php foreach ($data['productsData'] as $row): ?>
                        <input name="item_id" type="hidden" value="<?= $row->getId() ?>">
                    <?php endforeach ?>  
                    <select class="form-select" id="item_size" name="item_size" required>
                        <option value="" selected disabled hidden>Choose size</option>
                        <?php foreach ($data['sizeData'] as $row): ?>
                            <option value="<?= $row->getSize() ?>"><?= $row->getSize() ?></option>
                        <?php endforeach ?>
                    </select>
                    <select class="form-select" id="quantity" name="quantity" required>
                        <option value="" selected disabled hidden>Choose quantity</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                        <option value="4">Four</option>
                        <option value="5">Five</option>
                    </select><br>
                    <div class="row">             
                        <div class="col-3">
                            <input name="cartId" type="hidden" value="<?= $_SESSION['cartId'] ?>">
                            <a href="<?= URL; ?>CartItems/index"><button id="button-74" 
                                                                         type="submit" name="submit" value="submit" class="card button-74" >Add to Cart</button></a>
                        </div>     
                        <div class="col-3">
                            <p class="product-message"></p>
                        </div> 
                    </div>
                </form>   
            </div>  
        </div>
    </div>
</div>
