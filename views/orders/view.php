<h3>Shopping history</h3>
<table id="avaiable_prod">
    <tr>
        <th>Order id</th>
        <th>Date of purchase</th>
        <th>Status</th>
        <th>Payment type</th>
        <th>Total</th>
        <th>Details</th>
    </tr>
    <?php foreach ($data as $value): ?>
        <tr>
            <td><?= $value->getOrderId() ?></td>
            <td><?= date('m/d/Y', $value->getPaymentTime()) ?></td>
            <td><?= $value->getPaymentStatus() ?></td>
            <td><?= $value->getPaymentType() ?></td>
            <td><?= $value->getTotalCost() . ' $' ?></td>
            <td><a href="<?= URL . 'OrderDetails/index' . "/" . $value->getOrderId() ?>"> view</td>
        </tr>
    <?php endforeach ?>
</table>
