
<style>
    <link rel ="stylesheet" href ="<?= URL; ?>public/css/default.css" />
</style>

<div id="conteiner-checkout" class="conteiner">
    <center><h1 class="mb-3" >Checkout</h1></center>
    <form action="Checkout/index" method="post" class="row g-3">
        <div class="col-md-4">
            <label for="inputName" class="form-label">Name</label>
            <input type="text" class="form-control" id="inputName" name="name" placeholder="John" required>
        </div>
        <div class="col-md-4">
            <label for="inputSurname" class="form-label">Surname</label>
            <input type="text" class="form-control" id="inputSurname" name="surname" placeholder="Wayne" required>
        </div>
        <div class="col-5">
            <label for="inputAddress" class="form-label">Address</label>
            <input type="text" class="form-control" id="inputAddress" name="address" placeholder="Stream 33/3" required>
        </div>
        <div class="col-md-5">
            <label for="inputCity" class="form-label">City</label>
            <input type="text" class="form-control" id="inputCity" name="city" placeholder="Birmingham" required>
        </div>
        <div class="col-md-2">
            <label for="inputZip" class="form-label">Zip</label>
            <input type="text" class="form-control" id="inputZip" name="zip" placeholder="17-700" required>
        </div>
        <div class="col-md-6">
            <label for="inputEmail4" class="form-label">Email</label>
            <input type="email" class="form-control" id="inputEmail4" name="email" placeholder="deam@ox.eu" required>
        </div>
        <div class="col-md-6">
            <label for="inputPhoneNumber" class="form-label">Phone number</label>
            <input type="tel" class="form-control" id="inputZip" pattern="[0-9]{9}" name="phoneNumber" placeholder="123456789" required>
        </div>
        <div class="row">  
            <div id="payment-section" class="col">
                <h4 class="mb-3">Type of payment</h4>
                <div id="form-check-out" class="form-check">
                    <input class="form-check-input" type="radio" name="paymentType" id="bycard" value="card">
                    <label class="form-check-label" for="bycard">
                        Credit card
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="paymentType" id="byCOD" value="COD">
                    <label class="form-check-label" for="byCOD">
                        Cash on delivery
                    </label>
                </div> 
            </div>
        </div>
        <p id="info-byCOD"><i>Remember to  prepare the money</i></p>
        <div id="credit-card-row" class="row gy-3">
            <div class="col-md-6">
                <label for="cc-name" class="form-label">Name on card</label>
                <input type="text" class="form-control" id="cc-name" placeholder="John Wayne" required>
                <small class="text-muted">Full name as displayed on card</small>
                <div class="invalid-feedback">
                    Name on card is required
                </div>
            </div>
            <div class="col-md-6">
                <label for="cc-number" class="form-label">Credit card number</label>
                <input type="tel" class="form-control" id="cc-number" inputmode="numeric" pattern="[0-9]{16}" maxlength="16" placeholder="xxxx xxxx xxxx xxxx" required>
                <div class="invalid-feedback">
                    Credit card number is required
                </div>
            </div>
            <div class="col-md-3">
                <label for="cc-expiration" class="form-label">Expiration</label>
                <input type="text" class="form-control" id="cc-expiration" placeholder="06/27" maxlength="5" required>
                <div class="invalid-feedback">
                    Expiration date required
                </div>
            </div>
            <div class="col-md-3">
                <label for="cc-cvv" class="form-label">CVV</label>
                <input type="text" class="form-control" id="cc-cvv" placeholder="666" inputmode="numeric" pattern="[0-9]{3}" maxlength="3" required>
                <div class="invalid-feedback">
                    Security code required
                </div>
            </div>
        </div>
        <div>
            <input class="form-control" name="userId" type="hidden" value="<?= $_SESSION['cart_userId'] ?>">
            <input class="form-control" name="paymentStatus" type="hidden" value="payment verification ..." >
            <input class="form-control" name="paymentTime" type="hidden" value="<?= time() ?>" >
        </div>
        <div class="col-12">
            <button id="checkout-button" type="submit" value="submit" name="submit" class="btn btn-primary">Pay now <?= $_SESSION['totalCost'] . ' $' ?></button>
        </div>
    </form>
</div>
