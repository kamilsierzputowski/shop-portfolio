<style>
    <link rel ="stylesheet" href ="<?= URL; ?>public/css/default.css" />
</style>

<div id="conteiner-cart" class="center container">
    <center>
        <form id="login-form" class="col-12" action="Login/run" method="post">       
            <div>
                <h1>Sign in</h1>
            </div> 
            <div class="form-group">
                <input type="text" class="form-control" id="userLogin" name="login" value="" placeholder="email" required/>
            </div>
            <div class="form-group">
                <input type="password"  class="form-control" id="userPassword" name="password" placeholder="password" value="" required/>
            </div>
            <div class="form-group">
                <input id="button-login" class="button-74" type='submit' name='submit' value='submit'>
            </div>
            <p class="login-msg"></p>
            <div>
                <p>You are not member yet? <a href="" id="signUp">sign up</a></p>
            </div>
        </form>

        <form id="register" action="AddUser/index" method="post">  
            <div>
                <h1>Sign up</h1>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="login" name="login" placeholder="email" required>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="password" name="password" placeholder="password!!!!" required >
            </div>
            <div class="form-group">
                <input id="button-register" class="button-74" type='submit' name='submit' value='submit'>
            </div>  
            <div>
                <p class="register-msg"></p>
            </div>
            <div>
                <p>Already a member? <a href="" id="signIn">sign in</a></p>
            </div> 
        </form>
    </center> 
</div>
