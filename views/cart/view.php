<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    <link rel ="stylesheet" href ="<?= URL; ?>public/css/default.css"/>
</style>

<div id="conteiner-cart" class="conteiner">
    <div id="row-width" class="row">  
        <div class="col-4">
            <center><h1 id="cartH1"><a id="katalogs_product" href="<?= URL; ?>Category">Back to shopping</a></h1></center>
        </div>   
        <div class="col-4" >
            <center><h1 id="cartH1">Your shopping cart(<?= $data['total_value'] . "$" ?>)</h1></center>
        </div>
        <div class="col-4">
            <center><h1 id="cartH1"><a id="katalogs_product" href="<?php
                    if (Youtube\libs\Session::get('loggedIn') === true) {
                        echo URL . 'Checkout';
                    } else {
                        echo URL . 'Login';
                    }
                    ?>">Checkout</a></h1></center>
        </div>
    </div>
    
    <form action="Cart/delete" method="post">        
        <div id="row-width" class="row">            
                <?php foreach ($data['itemsData'] as $value): ?>           
                    <div class="col-4">
                        <center>
                            <img class="responsive" src="<?= URL; ?>/public/images/<?= $value['product']->getImg() ?>"
                                 alt="alt" style="width:350px;height:250"/><br>
                            <button class="btn-primary" type="submit" name="id" value="<?= $value['cartItems']->getCartItemsId() ?>"><i class="fa fa-trash"></i></button>
                            <br>
                            <a id="katalogs_product">Model: <?= $value['product']->getName() ?><br>
                                Price: <?= $value['product']->getPrice() ?><br>
                                Symbol: <?= $value['product']->getSymbol() ?><br>
                                Size: <?= $value['cartItems']->getItemSize() ?><br>
                                Quantity: <?= $value['cartItems']->getQuantity() ?></a>     
                        </center>
                    </div>          
                <?php endforeach ?>
        </div>  
    </form>  
</div>
