<!doctype html>
<html>
    <head>
        <title> The Greatest app </title> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link rel ="stylesheet" href ="<?= URL; ?>public/css/default.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <script src="<?= URL ?>public/js/newjavascript.js" ></script>
    </head>

    <body>
        <div id="conteiner">
            <div id="header" style="font-family:verdana;">
                <div id="head1">
                    <div id="header-row" class="row">
                        <div class="col">
                            <a class="c" href="<?= URL; ?>Index">Manage products</a>
                        </div>
                        <div class="col">
                            <a class="c" href="<?= URL; ?>Category">Category</a>
                        </div>
                        <div class="col">
                            <a class="c" href="<?= URL; ?>Orders">Orders</a>
                        </div>
                        <?php if (\Youtube\libs\Session::get('loggedIn') == true): ?>
                            <div class="col">
                                <a class="c" href="<?= URL; ?>Dashboard/logout">Logout</a>
                            </div>
                        <?php else: ?>
                            <div class="col">
                                <a class="c" href="<?= URL; ?>Login">Log in</a>
                            </div>
                        <?php endif; ?>
                        <div class="col">
                            <a id="cart-shake" href="<?= URL; ?>Cart" class="c">
                                Cart
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="content">
